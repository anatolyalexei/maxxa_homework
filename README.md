### Descripción de tarea

- Obtener el valor del dólar desde la web  https://www.bcentral.cl/  equivalente en pesos moneda nacional de chile.

[========]

### Solución:
```seq
Browser->FrontEnd Angular: Consulta valor 
FrontEnd Angular->BackEnd NodeJS: Consume API Service
BackEnd NodeJS-->FrontEnd Angular: Retorna Valor Actual 
FrontEnd Angular-> Browser: Muestra Valor Actual 
Note right of Browser: Solicita visualizar\nel valor actual 
Note right of BackEnd NodeJS: Ejecuta scrapper\na la web bcentral.cl 
```

[========]

### Características de la solución:
- Desarrollado en NodeJS y Angular. Lenguajes usados JavaScript y TypeScript ECMA 6.
- Se desarrolló un backend liviano en NodeJS, que hace de scraper data y tambien de publicar el resultado desde un método REST (API con ExpressJS).
- Tambien se desarrollo una aplicacion web como FrontEnd implementado en Angular  5.2 Stable y UI/UX en Bootstrap, que nos permite visualizar la data obtenida por scraping.
- El recurso como endpoint publicado desde el backEnd es: http://localhost:3000/api/value
- El acceso a la aplicación de consulta para este propósito es: http://localhost:4200/

[========]


##Pasos para correr la aplicación 
Todos los pasos se deben hacer desde consola o terminal, como pre-requisito debe tener instalado nodejs la versión reciente LTS (https://nodejs.org/es/download/), que trae consigo los binarios de node y npm o yarn, estos últimos 
####Clonar repositorio de código

`$ git clone [uri repo]`

Hay que ubicarse en el folder raíz de la solución para ejecutar los comandos, debe fijarse que esté el fichero packages.json, que es el manifest de toda app basado en nodejs y es la ubicación donse se creará automaticamente el folder node_modules, donde se alojarán todos los modulos de paquetes o dependencias. Seguir los siguientes pasos.

####BackEnd (NodeJS server)

1. Instalar las dependencias para el server nodejs como backend.

`$ npm install express --save` 

`$ npm install cheerio request cors`

2. Levantar el backend desde la consola o terminal, el cual hace binding en el puerto localhost:3000, que debe accederse desde browser.

`$ node server`

####FrontEnd (Angular)

1. Instalar todas las dependencias que por defecto usa una app en Angular.

`$ npm install`

2. Desde una nueva consola o terminal, debemos de compilar y correr el frontend, el cual hace binding en el puerto localhost:4200, accediendo desde browser.

`$ npm start`

 Y listo! 

Comentarios finales
=============
- Todo debe funcionar sin problemas, siempre y cuando tenga los 2 terminales ejecutándose según los pasos anteriores.
- Recordar que **nodejs** y **npm** nos ayudan con algunas tareas, además en la creación de los ficheros del **manifest** y agregar archivos en el proyecto. Así como tambien **Angular-CLI** nos permite crear una estructura de proyecto con la versión del angular core modules y demás módulos base de dependencias necesarios y/o agregar desde el mismo CLI nuevos ficheros a nuestro proyecto. Todo desde consola.

##Componentes y herramientas usadas
- Para ayudarnos en la exploración de documentos html y exploración DOM, para las tareas de scraper, se ha usado **Cheerio** (https://cheerio.js.org/)
- **ExpressJS**, que nos ofrece toda la infraestructura minimalizada y flexible para aplicaciones web Node.js, el cual nos proporciona un conjunto sólido de características para las aplicaciones web y móviles, incluyendo poder crear rápidamente sólidas APIs RESTFul (https://expressjs.com/es/)
- Se uso una versión estable de Angular 5.2.0, el cual me permitió avanzar sin problemas sin hacer breaking changes en el código con la infraestructura.
- Todo fue desarrollado en **Visual Studio Code** y con ayuda del navegador Google Chrome desde MacOS. 
###End